﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in); // tastatur ist eie Variable

        double zuZahlenderBetrag = 0; // (double)Variable, Operationen: User-Inut, Multiplikation mit den tickets, While loop welcher den restbetrag bestimmt...
        double eingezahlterGesamtbetrag = 0; // (double)Variable, Operationen: Variable wird mit = 0.0 definiert, While loop wird ausgefüht, bis der Wert von gesamteingezahlterbetrag nicht mehr kleiner als der gesamtbetrag ist. In dem loop wird dann das eingeworfende Geldstück zu dem eingezahlterGesamtbetrag addiert. Das Rückgeld wird ebenfalls mit eingezahlterGesamtbetrag - zuZahlenderBetrag definiert
        double eingeworfeneMünze = 0; // (double)Variable Operationen:
        double rückgabebetrag = 0; // (double)Variable, Operationen: rückgabebetrag wird mit eingezahlterGesamtbetrag - zuZahlenderBetrag berechnet, rückgabebetrag wird bei zu hohem münzeinwurf ausgegeben, mit den rückgabebetrag wird das bestmögliche Rückgeld berechnet
        byte tickets = 0; // (byte)Variable, Operationen: User-Input für die Anzahl der Tickets, zuzahlenderBetrag wird mit den tickets multipliziert, um den zuzahlenden  betrag aller tickets hat
        // ich habe mich für den Datentyp byte entschieden, das der nicht in den negativen bereich gehen kann, was in den fall sinnvoll ist

        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();

        while(tickets < 1 || tickets > 10) {
            System.out.print("Anzahl der Tickets (1 - 10): ");
            tickets = tastatur.nextByte();
        }


        zuZahlenderBetrag *= tickets;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
            System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
}